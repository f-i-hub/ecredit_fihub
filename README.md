# Весь F&I в одной платформе

Данная документация описывает часть сервисов F&I Hub eCredit: Кредит Онлайн, еОсаго, еКаско.

[Кредит Онлайн](credit_online/credit_online.md)

[еОсаго](eosago/eosago.md)

[еКаско](ekasko/ekasko.md)

[Доступ через виджет](widget/widget.md)