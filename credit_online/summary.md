# Общая информация

Цель данного сервиса в том, чтобы клиент мог самостоятельно через web форму составить заявку на покупку автомобиля в кредит.

При этом клиент будет видеть реальные предварительные кредитные предложения от разных банков, а также сможет узнать предварительное решение от банков.

Кредит Онлайн может работать в двух режимах.

**Короткая анкета** 

Цель данного режима состоит в том, чтобы предоставить клиенту  возможность максимально просто создать заявку на покупку автомобиля и получить обратный звонок по своей заявке.

Клиент выбирает автомобиль, сразу видит предварительные предложения по кредитам на разные периоды, заполняет имя и телефон, и данная информация передается в еКредит.

Далее клиент ожидает звонка представителя дилерского цента

**Полная анкета**

Цель данного режима состоит в том, чтобы клиент мог не только подать заявку на покупку автомобиля в кредит, но еще и в автоматическом режиме “здесь и сейчас” смог узнать предварительное решение по своему обращению.
В случае, если банк выдает отказ, то клиент увидит не отказ, а сообщение о том, что по его заявке принято решение (какое не указывается), и что с ним свяжется сотрудник дилерского центра. После этого кредитный специалист сможет продолжить работу с клиентом по другим сценариям.

Таким образом режим “Полная анкета” так же, как и “Короткая анкета” включает в себя создание заявки. Но еще и сохраняет полные данные клиента и выполняет автоматическую отправку заявки на кредит в банки.

## WEB адреса
Для каждого партнера доступен личный web адрес сервиса.

Имя сервиса стандартизировано и составляется так: **parner_name-online.e-credit.one**, где partner_name - уникальное имя партнера, например, название компании или основного продаваемого бренда.

Для каждого партнера доступна тестовая среда F&I Hub, адрес которой выглядит так: **parner_name-online-test.e-credit.one**. На тестовой среде можно проверить функционал без реальных отправок в банки.
